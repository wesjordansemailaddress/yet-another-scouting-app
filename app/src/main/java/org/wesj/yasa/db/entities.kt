package org.wesj.yasa.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.json.JSONObject

@Entity(tableName = "match")
data class MatchEntity (
    @PrimaryKey(autoGenerate = true) val match_id: Int,
    val team_id: Int,
    val matchData: String?, // JSON
    val completed: Boolean = false
) {
    override fun toString(): String = "[MATCH Q$match_id / TEAM $team_id] $matchData"

    val state get() = if (matchData == null ) {
        MATCH_STATE_CREATED
    } else {
        if (completed) {
            MATCH_STATE_COMPLETED
        } else {
            MATCH_STATE_EDITED
        }
    }

    val json: JSONObject get() = JSONObject(matchData ?: "{}")
        .put("match_id", match_id)
        .put("team_id", team_id)
        .put("completed", completed)

    companion object {
        const val MATCH_STATE_CREATED: Int = 1
        const val MATCH_STATE_EDITED: Int = 2
        const val MATCH_STATE_COMPLETED: Int = 3
    }
}