package org.wesj.yasa.db

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

val MIGRATION_1_2 = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE `match` ADD COLUMN `completed` TINYINT NOT NULL DEFAULT 0 ")
    }
}
