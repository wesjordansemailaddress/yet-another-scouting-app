package org.wesj.yasa.db

import android.util.Log
import androidx.lifecycle.LiveData

class Repository (private val matchDao: MatchDao) {
    val allMatches: LiveData<List<MatchEntity>> = matchDao.getMatches()

    companion object {
        val TAG = Repository::class.java.canonicalName
    }

    fun find(match_id: Int): LiveData<MatchEntity> = matchDao.findMatch(match_id)

    suspend fun allMatchesAsync() = matchDao.getMatchesAsync()

    suspend fun update(matchEntity: MatchEntity) {
        Log.i(TAG, "Updating existing match to database...")
        matchDao.updateMatch(matchEntity)
    }

    suspend fun insert(matchEntity: MatchEntity) {
        Log.i(TAG, "Writing new match to database...")
        matchDao.insertMatch(matchEntity)
    }

    suspend fun delete(matchId: Int) {
        Log.i(TAG, "Deleting match ID $matchId")
        matchDao.deleteMatch(matchId)
    }

    suspend fun deleteAllMatches() {
        Log.w(TAG, "*** Deleting All Matches ***")
        matchDao.deleteAll()
    }
}