package org.wesj.yasa.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MatchDao {
    @Query("SELECT * from `match` ORDER BY match_id")
    fun getMatches(): LiveData<List<MatchEntity>>

    @Query("SELECT * from `match` ORDER BY match_id")
    suspend fun getMatchesAsync(): List<MatchEntity>

    @Query("SELECT * FROM `match` WHERE match_id = :match_id LIMIT 1")
    fun findMatch(match_id: Int): LiveData<MatchEntity>

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun updateMatch(matchEntity: MatchEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMatch(m: MatchEntity)

    @Delete
    suspend fun deleteMatch(m: MatchEntity)

    @Query("DELETE FROM `match` WHERE match_id = :id")
    suspend fun deleteMatch(id: Int)

    @Query("DELETE FROM `match`")
    suspend fun deleteAll()
}