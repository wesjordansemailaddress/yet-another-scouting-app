package org.wesj.yasa.ui.match

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.wesj.yasa.db.AppDatabase
import org.wesj.yasa.db.MatchEntity
import org.wesj.yasa.db.Repository

class MatchEditViewModel(application: Application) : AndroidViewModel(application) {
    // The ViewModel maintains a reference to the repository to get data.
    private val repository: Repository

    // LiveData gives us updated matches when they change.
    private var theMatch: LiveData<MatchEntity>? = null

    companion object {
        val TAG = MatchEditViewModel::class.java.canonicalName
    }

    init {
        // Gets reference to MatchDao from AppDatabase to construct
        // the correct Repository.
        val matchDao = AppDatabase.getDatabase(application, viewModelScope).matchDao()
        repository = Repository(matchDao)
    }

    fun setMatch(match_id: Int): LiveData<MatchEntity> {
        val m = repository.find(match_id)
        theMatch = m
        return m
    }

    fun getMatch() = theMatch

    fun update(match: MatchEntity) = viewModelScope.launch {
        Log.i(TAG, theMatch?.value?.toString())
        repository.update(match)
    }

    fun resetMatchData() {
        updateMatch { old ->
            MatchEntity(old.match_id, old.team_id, null, old.completed)
        }
    }

    fun setMatchCompleted(isCompleted: Boolean) {
        updateMatch { old ->
            MatchEntity(old.match_id, old.team_id, old.matchData, isCompleted)
        }
    }

    override fun onCleared() {
        super.onCleared()
        Log.d(TAG, "Viewmodel cleared!")
    }

    inline fun updateMatch(block: (MatchEntity)->MatchEntity): Job? {
        val b = getMatch()?.value?.let(block)
        return if (b != null) update(b) else null
    }
}
