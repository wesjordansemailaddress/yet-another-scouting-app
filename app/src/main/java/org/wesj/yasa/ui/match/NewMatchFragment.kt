package org.wesj.yasa.ui.match

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import org.wesj.yasa.R
import org.wesj.yasa.db.MatchEntity

class NewMatchFragment : Fragment() {
    private lateinit var edittextMatchNum: EditText
    private lateinit var edittextTeamNum: EditText

    private val viewModel: NewMatchViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.match_create_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        edittextMatchNum = view.findViewById(R.id.edit_matchnum)
        edittextTeamNum = view.findViewById(R.id.edit_teamnum)

        val button = view.findViewById<Button>(R.id.btn_save)!!
        button.setOnClickListener {
            buildMatchFromUI()?.let { match ->
                val action = NewMatchFragmentDirections.actionNewMatchFragmentToMatchListFragment()
                viewModel.saveMatch(match)
                view.findNavController().navigate(action)
            }
        }
    }

    private fun buildMatchFromUI(): MatchEntity? {
        val teamNum = edittextTeamNum.text.toString().toIntOrNull()
        val matchNum = edittextMatchNum.text.toString().toIntOrNull()

        if (teamNum == null) {
            Toast.makeText(activity, "Invalid team number!", Toast.LENGTH_SHORT).show()
            return null
        }

        if (matchNum == null) {
            Toast.makeText(activity, "Invalid match number!", Toast.LENGTH_SHORT).show()
            return null
        }

        // TODO: primary key checking

        return MatchEntity(matchNum, teamNum, null)
    }
}
