package org.wesj.yasa.ui.management

object PreferenceKeys {
    const val CONFIGURE_SCORESHEET_DIR: String ="configure_scoresheet_dir"
    const val EXPORT_UPLOAD: String = "export_upload"
    const val EXPORT_UPLOAD_NOW: String = "export_upload_now"
    const val EXPORT_UPLOAD_URL: String = "export_upload_url"
    const val EXPORT_UPLOAD_AUTO: String = "export_upload_auto"

    const val CONFIGURE_RESET: String = "configure_reset"

    const val SCHEDULE_DELETE: String = "schedule_delete"
}