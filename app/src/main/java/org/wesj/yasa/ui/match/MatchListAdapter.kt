package org.wesj.yasa.ui.match

import android.content.Context
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.wesj.yasa.R
import org.wesj.yasa.db.MatchEntity

class MatchListAdapter
internal constructor(context: Context, val listener: InteractionListener) :
    RecyclerView.Adapter<MatchListAdapter.MatchViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var matches = emptyList<MatchEntity>() // cached copy of matches

    override fun getItemCount(): Int = matches.size

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bind(matches[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return MatchViewHolder(itemView)
    }

    internal fun setMatches(matches: List<MatchEntity>) {
        this.matches = matches
        notifyDataSetChanged()
    }

    inner class MatchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        MenuItem.OnMenuItemClickListener, View.OnClickListener {


        var match_id: Int? = null
        val matchItemView: TextView = itemView.findViewById(R.id.rv_matchnum)
        val matchTeamView: TextView = itemView.findViewById(R.id.rv_teamnum)
        val matchStatusView: ImageView = itemView.findViewById(R.id.rv_matchstate)

        init {
            itemView.apply {
                setOnClickListener(this@MatchViewHolder)
                setOnCreateContextMenuListener { menu, _, _ ->
                    menu.add(0, itemView.id, 1, "Delete Match Q$match_id")
                        .setOnMenuItemClickListener(this@MatchViewHolder)
                }
            }
        }

        override fun onClick(v: View?) {
            match_id?.let { listener.onSelectItem(itemView, it) }
        }

        override fun onMenuItemClick(item: MenuItem?): Boolean {
            match_id?.let { listener.onDeleteItem(itemView, it) }
            return true
        }

        fun bind(match: MatchEntity) {
            match_id = match.match_id
            matchItemView.text = "Match Q${match.match_id}"
            matchTeamView.text = "Team ${match.team_id}"

            val statusId = when (match.state) {
                MatchEntity.MATCH_STATE_COMPLETED -> R.drawable.ic_circle_good_24dp
                MatchEntity.MATCH_STATE_EDITED -> R.drawable.ic_circle_inprogress_24dp
                MatchEntity.MATCH_STATE_CREATED -> R.drawable.ic_circle_bad_24dp
                else -> R.drawable.ic_circle_bad_24dp
            }

            matchStatusView.setImageResource(statusId)
        }
    }

    interface InteractionListener {
        fun onSelectItem(v: View, match_id: Int)
        fun onDeleteItem(v: View, match_id: Int)
    }
}