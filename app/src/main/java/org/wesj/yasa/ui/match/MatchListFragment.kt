package org.wesj.yasa.ui.match

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.match_index_fragment.view.*
import org.wesj.yasa.R


class MatchListFragment : Fragment(), MatchListAdapter.InteractionListener {

    companion object {
        val TAG = MatchListFragment::class.java.canonicalName
    }

    private val listViewModel: MatchListViewModel by viewModels()
    private lateinit var adapter: MatchListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.match_index_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val ctx = this.requireContext()

        val rv = view.findViewById<RecyclerView>(R.id.recyclerview)
        val btn = view.findViewById<FloatingActionButton>(R.id.fab)

        adapter = MatchListAdapter(ctx, this)
        val lm = LinearLayoutManager(ctx)
        rv.adapter = adapter
        rv.layoutManager = lm

        val dividerItemDecoration = DividerItemDecoration(rv.context, lm.orientation)
        rv.addItemDecoration(dividerItemDecoration)

        btn.fab.setOnClickListener { button ->
            val action = MatchListFragmentDirections
                .actionMatchListFragmentToNewMatchFragment()
            button.findNavController().navigate(action)
        }

        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        menu.apply {
            add("Management").apply {
                setIcon(R.drawable.ic_assignment_white_24dp)
                setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)

                setOnMenuItemClickListener {
                    val action = MatchListFragmentDirections.actionMatchListFragmentToManagementFragment()
                    findNavController().navigate(action)
                    true
                }
            }
        }
    }

    /*
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.title) {
            "Management" -> {
                val action = MatchListFragmentDirections
                    .actionMatchListFragmentToManagementFragment()
                findNavController().navigate(action)
                true
            }
            else -> true
        }
    }*/

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        listViewModel.allMatches.observe(viewLifecycleOwner, Observer { matches ->
            if (matches != null) {
                adapter.setMatches(matches)
            } else {
                Log.wtf(TAG, "Matches list is null???")
            }
        })
    }

    override fun onSelectItem(v: View, match_id: Int) {
        val action = MatchListFragmentDirections
            .actionMatchListFragmentToMatchEditFragment(match_id)
        v.findNavController()
            .navigate(action)
    }

    override fun onDeleteItem(v: View, match_id: Int) {
        val confirmationDialog = activity?.let {
            AlertDialog.Builder(it)
                .setTitle("Delete Match Q$match_id?")
                .setMessage("Really delete this match and its data? This cannot be undone.")
                .setPositiveButton("Delete") { _, _ -> listViewModel.delete(match_id) }
                .setNegativeButton("Cancel") { _, _ -> }
                .create()
        }

        confirmationDialog?.show()
    }
}