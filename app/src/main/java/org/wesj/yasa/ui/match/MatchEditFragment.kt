package org.wesj.yasa.ui.match

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.JavascriptInterface
import android.webkit.WebView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.preference.PreferenceManager
import kotlinx.coroutines.launch
import org.wesj.yasa.*
import org.wesj.yasa.db.MatchEntity
import org.wesj.yasa.lib.askForConfirmation
import org.wesj.yasa.lib.requestStoragePerms
import org.wesj.yasa.lib.setToolbarTitle
import org.wesj.yasa.lib.showConfirmation
import org.wesj.yasa.ui.management.PreferenceKeys
import org.wesj.yasa.work.ScoresheetLoader


class MatchEditFragment : Fragment() {

    companion object {
        val TAG = MatchEditFragment::class.java.canonicalName
    }

    private val args: MatchEditFragmentArgs by navArgs()
    private val viewModel: MatchEditViewModel by viewModels()

    private var webview: WebView? = null
    private var isCompletedMenuItem: MenuItem? = null

    private lateinit var jsInterface: WebviewJSInterface

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.setMatch(args.matchId).observe(viewLifecycleOwner, Observer { bindData(it) })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.match_edit_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadScoreSheet(view)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.apply {
            add("Change Team").apply {
                setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER)
                setOnMenuItemClickListener {
                    // TODO
                    Log.i(TAG, "Menu press \"Change Team\"")
                    false
                }
            }

            add("Change Match Number").apply {
                setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER)
                setOnMenuItemClickListener {
                    // TODO
                    Log.i(TAG, "Menu press \"Change Match Number\"")
                    false
                }
            }

            add("Clear Data").apply {
                setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER)
                setOnMenuItemClickListener {
                    val ctx = requireContext()
                    ctx.askForConfirmation("Really clear data?", "This can't be undone.") {
                        Log.i(TAG, "Match data reset.")
                        viewModel.resetMatchData()
                        findNavController().popBackStack()
                        ctx.showConfirmation("Match data reset")
                    }
                    true
                }
            }

            isCompletedMenuItem = add("Completed").apply {
                setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER)
                isCheckable = true
                setOnMenuItemClickListener {
                    Log.d(TAG, "Old checked state: $isChecked")
                    isChecked = !isChecked

                    Log.i(TAG, "Match completed set to: $isChecked")
                    viewModel.setMatchCompleted(isChecked)
                    true
                }
            }

            add("Done").apply {
                setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
                setOnMenuItemClickListener {
                    Log.i(TAG, "Match marked as done")
                    viewModel.setMatchCompleted(true)
                    findNavController().popBackStack()
                    true
                }
            }
        }
    }

    private fun bindData(loaded: MatchEntity) {
        // Note: this gets called every time saveData() is called
        Log.i(TAG, "Bound match $loaded")
        jsInterface.match = loaded
        isCompletedMenuItem?.isChecked = loaded.completed
        setToolbarTitle("Match Q${loaded.match_id} | Team ${loaded.team_id}")
    }

    private fun loadScoreSheet(v: View) {
        val dir = PreferenceManager
            .getDefaultSharedPreferences(context)
            .getString(PreferenceKeys.CONFIGURE_SCORESHEET_DIR, "/yasa_scoresheets")!!

        val loader = ScoresheetLoader(dir)

        jsInterface = WebviewJSInterface(v.context)
        webview = v.findViewById<WebView>(R.id.match_edit_webview)?.apply {
            addJavascriptInterface(jsInterface, "YASA")
            activity?.requestStoragePerms {
                loader.scope.launch {
                    loader.useWebView(this@apply)
                }
            }
        }
    }

    inner class WebviewJSInterface(
        private val context: Context
    ) {
        internal var match: MatchEntity? = null

        @JavascriptInterface
        fun saveData(json: String?) {
            val matchNew = match?.run {
                MatchEntity(match_id, team_id, json ?: matchData)
            }
            if (matchNew != match && matchNew != null) {
                viewModel.update(matchNew)
            } else {
                Log.i(TAG, "Data is the same, not updating")
            }
        }

        @JavascriptInterface
        fun loadData(): String? = match?.matchData

        @JavascriptInterface
        fun teamNumber(): Int? = match?.team_id

        @JavascriptInterface
        fun matchNumber(): Int? = match?.match_id

        @JavascriptInterface
        fun apiVersion(): String = "1.0"
    }

}
