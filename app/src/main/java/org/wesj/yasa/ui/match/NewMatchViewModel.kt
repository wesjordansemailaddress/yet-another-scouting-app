package org.wesj.yasa.ui.match

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.wesj.yasa.db.AppDatabase
import org.wesj.yasa.db.MatchEntity
import org.wesj.yasa.db.Repository

class NewMatchViewModel(private val app: Application): AndroidViewModel(app) {
    // The ViewModel maintains a reference to the repository to get data.
    private val repository: Repository

    companion object {
        val TAG = MatchEditViewModel::class.java.canonicalName
    }

    init {
        // Gets reference to MatchDao from AppDatabase to construct
        // the correct Repository.
        val matchDao = AppDatabase.getDatabase(app, viewModelScope).matchDao()
        repository = Repository(matchDao)
    }

    fun saveMatch(entity: MatchEntity) {
        viewModelScope.launch {
            Log.i(TAG, "Saving new match: $entity")
            repository.insert(entity)
        }
    }
}