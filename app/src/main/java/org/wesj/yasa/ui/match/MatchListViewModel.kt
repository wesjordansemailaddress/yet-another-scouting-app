package org.wesj.yasa.ui.match

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.wesj.yasa.db.AppDatabase
import org.wesj.yasa.db.MatchEntity
import org.wesj.yasa.db.Repository

class MatchListViewModel(application: Application): AndroidViewModel(application) {

    // The ViewModel maintains a reference to the repository to get data.
    private val repository: Repository

    // LiveData gives us updated matches when they change.
    val allMatches: LiveData<List<MatchEntity>>

    init {
        // Gets reference to MatchDao from AppDatabase to construct
        // the correct Repository.
        val matchDao = AppDatabase.getDatabase(application, viewModelScope).matchDao()
        repository = Repository(matchDao)
        allMatches = repository.allMatches
    }

    /**
     * The implementation of insert() in the database is completely hidden from the UI.
     * Room ensures that you're not doing any long running operations on
     * the main thread, blocking the UI, so we don't need to handle changing Dispatchers.
     * ViewModels have a coroutine scope based on their lifecycle called
     * viewModelScope which we can use here.
     */
    fun insert(matchEntity: MatchEntity) = viewModelScope.launch {
        repository.insert(matchEntity)
    }

    fun delete(matchId: Int) = viewModelScope.launch {
        repository.delete(matchId)
    }

}