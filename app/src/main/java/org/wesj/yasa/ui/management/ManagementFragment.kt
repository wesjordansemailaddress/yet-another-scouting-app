package org.wesj.yasa.ui.management

import android.os.Bundle
import androidx.preference.*
import org.wesj.yasa.R
import org.wesj.yasa.lib.askForConfirmation
import org.wesj.yasa.lib.requestStoragePerms
import org.wesj.yasa.lib.settings_ext.category
import org.wesj.yasa.lib.settings_ext.checkBox
import org.wesj.yasa.lib.settings_ext.editText
import org.wesj.yasa.lib.settings_ext.preference
import org.wesj.yasa.lib.settings_ext.preferences
import org.wesj.yasa.lib.settings_ext.setOnStringPreferenceChange
import org.wesj.yasa.work.ManagementActions

class ManagementFragment : PreferenceFragmentCompat() {
    companion object {
        val TAG = ManagementFragment::class.java.canonicalName
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        val actions = ManagementActions(requireActivity().application)

        preferenceScreen = preferences {
            category("Export Data") {
                setIcon(R.drawable.ic_import_export_accent_24dp)

                preference("Export to file") {
                    setIcon(R.drawable.ic_sd_storage_black_24dp)
                    summary = "Export scouting data to device SD card."

                    setOnPreferenceClickListener {
                        requireActivity().requestStoragePerms {
                            actions.exportDataToSDCard()
                        }
                        true
                    }
                }

                editText("Set Upload URL", PreferenceKeys.EXPORT_UPLOAD_URL) {
                    setIcon(R.drawable.ic_cloud_upload_black_24dp)
                    summaryProvider = EditTextPreference.SimpleSummaryProvider.getInstance()
                }

                preference("Upload Now") {
                    setIcon(R.drawable.ic_sync_black_24dp)
                    summary = "Internet connection is required."
                    dependency = PreferenceKeys.EXPORT_UPLOAD_URL

                    setOnPreferenceClickListener {
                        actions.exportDataToURL()
                        true
                    }
                }

                checkBox("Export Automatically", PreferenceKeys.EXPORT_UPLOAD_AUTO) {
                    summary = "Automatically export after every completed match."
                    isEnabled = false
                }
            }

            category("Device Configuration") {
                setIcon(R.drawable.ic_settings_applications_accent_24dp)

                preference("Start New Event...") {
                    setIcon(R.drawable.ic_create_new_folder_black_24dp)
                    summary = "Run event configuration wizard."
                    isEnabled = false
                }

                editText("Set Scoresheet Directory", PreferenceKeys.CONFIGURE_SCORESHEET_DIR) {
                    setIcon(R.drawable.ic_code_black_24dp)
                    setDefaultValue("yasa_scoresheets")

                    summaryProvider = EditTextPreference.SimpleSummaryProvider.getInstance()

                    setOnStringPreferenceChange { newValue ->
                        activity?.requestStoragePerms {
                            actions.validateScoresheetDirectory(newValue)
                            true
                        } ?: false
                    }

                    /*
                    setOnPreferenceChangeListener { _, newValue ->
                        activity?.requestStoragePerms {
                            (newValue as? String)?.let {
                                actions.validateScoresheetDirectory(it)
                                true
                            } ?: false
                        } ?: false
                    }

                     */
                }
            }

            category("Match Schedule") {
                setIcon(R.drawable.ic_format_list_numbered_accent_24dp)

                preference("Import match schedule from JSON") {
                    setIcon(R.drawable.ic_sd_storage_black_24dp)
                    summary = "Preload match schedule from SD card."
                    isEnabled = false
                }

                preference("Import match schedule from TBA") {
                    setIcon(R.drawable.ic_tba_lamp_black_24dp)
                    summary = "Preload match schedule from The Blue Alliance."
                    isEnabled = false
                }

                preference("Delete All Matches") {
                    setIcon(R.drawable.ic_delete_sweep_black_24dp)
                    summary = "Delete all matches and scouting data."

                    setOnPreferenceClickListener {
                        context.askForConfirmation("Delete Matches",
                            "Really delete all matches AND DATA? This can't be undone.") {
                            actions.resetMatches()
                        }
                        true
                    }
                }
            }
        }
    }
}
