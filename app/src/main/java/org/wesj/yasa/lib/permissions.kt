package org.wesj.yasa.lib

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

const val PERMISSIONS_REQUEST_EXTERNAL_STORAGE: Int = 1

fun <T> Activity.requestStoragePerms(then: () -> T?): T? {
    val TAG = this::class.java.canonicalName!!

// Here, thisActivity is the current activity
    if (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        != PackageManager.PERMISSION_GRANTED
    ) {
        Log.i(TAG, "Storage perms not granted, requesting permission")
        // Permission is not granted
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            Log.i(TAG, "Showing rationale for storage permissions")
            // TODO
        } else {
            // No explanation needed, we can request the permission.
            Log.i(TAG, "Requesting storage permissions")
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                PERMISSIONS_REQUEST_EXTERNAL_STORAGE
            )

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    } else {
        // Permission has already been granted
        Log.i(TAG, "Storage permissions already granted")
    }

    return if (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        == PackageManager.PERMISSION_GRANTED
    ) {
        Log.i(TAG, "Storage perms allowed, running clause")
        then()
    }
    else {
        Log.w(TAG, "Storage perms denied! Returning null.")
        null
    }

}

