package org.wesj.yasa.lib

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

interface CanSetToolbarTitle {
    var toolbarTitle: String?
}
