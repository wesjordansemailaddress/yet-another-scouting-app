package org.wesj.yasa.lib

import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*

private const val TAG = "work.fileUtils"

fun File.writeJSONObject(json: JSONObject) {
    Log.i(TAG, "Exporting data to: ${this.path}")
    val jsonArrayStr = json.toString(2)

    if (this.exists()) {
        this.delete()
        Log.w(TAG, "Found existing file, truncating...")
    }
    this.createNewFile()


    BufferedWriter(FileWriter(this)).use { out ->
        Log.i(TAG, "Exporting JSON: $jsonArrayStr")
        out.write(jsonArrayStr)
    }

    Log.d(TAG, "Finished writing data.")
}

fun File.writeJSONArray(json: JSONArray) {
    Log.i(TAG, "Exporting data to: ${this.path}")
    val jsonArrayStr = json.toString(2)

    if (this.exists()) {
        this.delete()
        Log.w(TAG, "Found existing file, truncating...")
    }
    this.createNewFile()

    BufferedWriter(FileWriter(this)).use { out ->
        Log.i(TAG, "Exporting JSON: $jsonArrayStr")
        out.write(jsonArrayStr)
    }

    Log.d(TAG, "Finished writing data.")
}

fun File.readJSONObject(): JSONObject? = try {
    val text = FileReader(this).use { it.readText() }
    text.toJsonObject()
} catch (io: IOException) {
    Log.e(TAG, "Failed to read file: $path", io)
    null
} catch (json: JSONException) {
    Log.e(TAG, "Failed to parse JSONObject from file: $path")
    null
}

fun File.readJSONArray(): JSONArray? = try {
    val text = FileReader(this).use { it.readText() }
    text.toJsonArray()
} catch (io: IOException) {
    Log.e(TAG, "Failed to read file: $path", io)
    null
} catch (json: JSONException) {
    Log.e(TAG, "Failed to parse JSONArray from file: $path")
    null
}