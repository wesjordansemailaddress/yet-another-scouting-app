package org.wesj.yasa.lib

import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.preference.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

fun Fragment.setToolbarTitle(title: String) {
    (activity as CanSetToolbarTitle?)?.toolbarTitle = title
}

fun String.toJsonObject(): JSONObject? = try {
    JSONObject(this)
} catch (e: JSONException) {
    Log.w("String", "JSON object parse failed: $this", e)
    null
}

fun String.toJsonArray(): JSONArray? = try {
    JSONArray(this)
} catch (e: JSONException) {
    Log.w("String", "JSON array parse failed: $this", e)
    null
}

fun Int.getStatusCodeClass(): Int = (this / 100) * 100

fun Context.askForConfirmation(title: String, message: String, andThen: () -> Unit) {
    AlertDialog
        .Builder(this).apply {
            setTitle(title)
            setMessage(message)
            setPositiveButton("Yes") { d, _ ->
                d.dismiss()
                 andThen()
            }

            setNegativeButton("No") { d, _ ->
                d.dismiss()
            }
        }.create()
        .show()
}

fun Context.showConfirmation(text: CharSequence) = Toast
    .makeText(this, text, Toast.LENGTH_SHORT)
    .show()

suspend fun Context.showAsyncConfirmation(text: CharSequence) = coroutineScope {
    launch(Dispatchers.Main) {
        this@showAsyncConfirmation.showConfirmation(text)
    }
}

object settings_ext {
    inline fun PreferenceGroup.preference(title: CharSequence, block: Preference.() -> Unit): Preference {
        val pref = Preference(context)
        addPreference(pref)
        pref.title = title
        pref.apply(block)
        return pref
    }

    inline fun PreferenceFragmentCompat.preferences(block: PreferenceScreen.() -> Unit): PreferenceScreen {
        val screen = preferenceManager.createPreferenceScreen(context)
        preferenceScreen = screen
        screen.apply(block)
        return screen
    }

    inline fun PreferenceGroup.editText(title: CharSequence, key: String, block: EditTextPreference.() -> Unit): EditTextPreference {
        val pref = EditTextPreference(context)
        pref.title = title
        pref.key = key
        pref.apply(block)
        addPreference(pref)
        return pref
    }

    inline fun PreferenceGroup.checkBox(title: CharSequence, key: String, block: CheckBoxPreference.() -> Unit): CheckBoxPreference {
        val pref = CheckBoxPreference(context)
        pref.title = title
        pref.key = key
        pref.apply(block)
        addPreference(pref)
        return pref
    }

    inline fun PreferenceGroup.category(title: CharSequence, block: PreferenceCategory.() -> Unit): PreferenceCategory {
        val pref = PreferenceCategory(context)
        addPreference(pref)
        pref.title = title
        pref.apply(block)
        return pref
    }

    fun EditTextPreference.setOnStringPreferenceChange(block: EditTextPreference.(String)->Boolean) {
        setOnPreferenceChangeListener { preference, newValue ->
            (newValue as? String)?.let {
                this.block(it)
            } ?: false
        }
    }
}

