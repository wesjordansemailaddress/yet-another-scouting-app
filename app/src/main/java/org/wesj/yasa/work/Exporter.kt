package org.wesj.yasa.work

import android.app.Application
import android.os.Environment
import android.util.Log
import android.widget.Toast
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.coroutines.awaitStringResponse
import kotlinx.coroutines.*
import org.json.JSONArray
import org.wesj.yasa.db.AppDatabase
import org.wesj.yasa.db.MatchEntity
import org.wesj.yasa.db.Repository
import org.wesj.yasa.lib.getStatusCodeClass
import org.wesj.yasa.lib.writeJSONArray
import java.io.*

class Exporter(private val application: Application) {

    companion object {
        val TAG = Exporter::class.java.canonicalName
    }

    val scope = CoroutineScope(Dispatchers.IO)
    val repo by lazy {
        val matchDao = AppDatabase.getDatabase(application, scope).matchDao()
        Repository(matchDao)
    }

    private val exportsDir: File by lazy {
        val dir = File(Environment.getExternalStorageDirectory(), "/yasa_exports")
        dir.mkdirs()
        Log.d(TAG, "Using export directory ${dir.path}")
        dir
    }


    fun <T> async(block: suspend CoroutineScope.(Exporter) -> T) =
        scope.async { block(this@Exporter) }

    fun launch(block: suspend CoroutineScope.(Exporter) -> Unit) =
        scope.launch { block(this@Exporter) }

    suspend fun exportExternalStorage(matchCompletion: Int = MatchEntity.MATCH_STATE_COMPLETED): File =
        coroutineScope {
            val jsonArray = getMatchesForExport(matchCompletion)
            val writeToFileJob = async {
                val file = File(exportsDir, "/export.json") // TODO better filenames
                file.writeJSONArray(jsonArray)
                showConfirmation("Data exported to ${file.path} successfully.")
                file
            }

            writeToFileJob.await()
        }

    suspend fun exportUpload(url: String, matchCompletion: Int = MatchEntity.MATCH_STATE_COMPLETED): Boolean = try {
            Log.i(TAG, "Trying to upload data to $url..")

            val jsonArray = getMatchesForExport(matchCompletion)
            val fuelReq = Fuel
                .post(url)
                .jsonBody(jsonArray.toString())
                .awaitStringResponse()

            val code = fuelReq.second.statusCode

            when (code.getStatusCodeClass()) {
                200 -> {
                    Log.i(TAG, " $url returned [${code}] ${fuelReq.third}")
                    showConfirmation("Cloud upload successful!")
                    true
                }
                else -> {
                    Log.w(TAG, " $url returned [${code}] ${fuelReq.third}")
                    showConfirmation("FAIL: Server $url responded with status code ${code}")
                    false
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, "Connection to $url failed.", e)
            showConfirmation("Failed to connect to $url.")
            false
        }

    private suspend fun getMatchesForExport(matchCompletion: Int) = JSONArray().apply {
        repo.allMatchesAsync()
            .filter { it.state >= matchCompletion }
            .forEach { put(it.json) }
    }

    private suspend fun showConfirmation(text: String) =
        coroutineScope {
            launch(Dispatchers.Main) {
                Toast.makeText(
                    application.applicationContext,
                    text,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
}