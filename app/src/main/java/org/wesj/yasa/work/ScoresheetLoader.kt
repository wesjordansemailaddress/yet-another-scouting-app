package org.wesj.yasa.work

import android.annotation.SuppressLint
import android.os.Environment
import android.util.Log
import android.webkit.WebView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

class ScoresheetLoader(scoresheetDirectory: String) {
    companion object {
        val TAG = ScoresheetLoader::class.java.canonicalName
        const val SCORESHEET_FILE = "scoresheet.html"
        const val BACKUP_SCORESHEET = "file:///android_asset/scoresheet_dne.html"
    }

    val scope = CoroutineScope(Dispatchers.IO)
    val file = File(Environment.getExternalStorageDirectory(), "/$scoresheetDirectory")

    suspend fun exists(): Boolean = withContext(scope.coroutineContext) {
        when {
            !file.exists() -> {
                Log.e(TAG, "Scoresheet dir ${file.path} does not exist!")
                false
            }
            !File(file, "/$SCORESHEET_FILE").exists() -> {
                Log.e(TAG, "Scoresheet page ${file.path}/$SCORESHEET_FILE does not exist!")
                false
            }
            else -> true
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    suspend fun useWebView(wv: WebView): WebView {
        if (exists()) {
            withContext(Dispatchers.Main) {
                Log.i(TAG, "Using scoresheet: ${file.path}/$SCORESHEET_FILE")
                wv.settings.allowContentAccess = true
                wv.settings.javaScriptEnabled = true
                wv.loadUrl("file:///${file.path}/$SCORESHEET_FILE")
            }
        } else {
            withContext(Dispatchers.Main) {
                Log.e(TAG, "Loading backup scoresheet.")
                wv.loadUrl(BACKUP_SCORESHEET)
            }
        }

        return wv
    }
}