package org.wesj.yasa.work

import android.app.Application
import android.os.Environment
import android.util.Log
import androidx.preference.PreferenceManager
import kotlinx.coroutines.*
import org.wesj.yasa.db.AppDatabase
import org.wesj.yasa.db.MatchEntity
import org.wesj.yasa.db.Repository
import org.wesj.yasa.lib.showAsyncConfirmation
import org.wesj.yasa.lib.showConfirmation
import org.wesj.yasa.ui.management.ManagementFragment
import java.io.File

class ManagementActions(val app: Application) {
    val scope = CoroutineScope(Dispatchers.IO)
    val repo by lazy {
        val matchDao = AppDatabase.getDatabase(app, scope).matchDao()
        Repository(matchDao)
    }

    fun exportDataToSDCard(): Job? = Exporter(app).launch { exporter ->
        exporter.exportExternalStorage()
    }

    fun exportDataToURL() {
        val url = PreferenceManager
            .getDefaultSharedPreferences(app.applicationContext)
            .getString("export_upload_url", "localhost")!!

        Exporter(app).launch { exporter ->
            exporter.exportUpload(url, matchCompletion = MatchEntity.MATCH_STATE_COMPLETED)
        }
    }

    fun validateScoresheetDirectory(dir: String) {
        val file = File(Environment.getExternalStorageDirectory(), dir)
        Log.i(ManagementFragment.TAG, "Updated scoresheet directory to ${file.path}")

        if (file.exists()) {
            app.applicationContext.showConfirmation("Updated scoresheet directory to ${file.path}")
        } else {
            app.applicationContext.showConfirmation("Could not find ${file.path} on device.")
        }

    }

    fun resetMatches() {
        scope.launch {
            repo.deleteAllMatches()
            app.applicationContext
        }
    }

    fun resetAll() {
        scope.launch {
            // TODO reset more things
            repo.deleteAllMatches()
            app.applicationContext.showAsyncConfirmation("Reset application.")
        }
    }
}