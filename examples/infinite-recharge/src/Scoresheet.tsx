import React = require("react");
import { Counter } from "./Counter";
import { Log } from "./util";

import ToggleButtonGroup from "react-bootstrap/ToggleButtonGroup";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import ToggleButton from "react-bootstrap/ToggleButton";

const TAG = "src/Scoresheet.tsx"

export interface Props {
    getState?: () => State | null,
    onChange: (newState: State) => void
}

// export interface State {
//     hatches: number,
//     cargo: number,
//     startLevel: number,
//     endLevel: number
// }

export enum ClimbResult {
    None = "NONE",
    Park = "PARK",
    ClimbMissed = "CLIMB_MISS",
    ClimbSuccess = "CLIMB_SUCCESS"
}

export interface State {
    autoBallsIn: number,
    autoLowGoal: number,
    autoOuterGoal: number,
    autoInnerGoal: number,
    autoBallsMissed: number,
    teleopBallsIn: number,
    teleopLowGoal: number,
    teleopOuterGoal: number,
    teleopInnerGoal: number,
    teleopBallsMissed: number,
    climb: ClimbResult
}

export class Scoresheet extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        if (props.getState) {
            const importedState = props.getState();
            if (importedState) {
                this.state = importedState
                return
            }
        }

        this.state = defaultState()
    }

    handleChange(newState: State) {
        Log.i(TAG, newState)
        this.props.onChange(newState)
        this.setState(newState)
    }

    setAutoBallsIn = (x: number) => { this.handleChange({...this.state, autoBallsIn: x}); }
    setAutoLowGoal = (x: number) => { this.handleChange({...this.state, autoLowGoal: x}); }
    setAutoOuterGoal = (x: number) => { this.handleChange({...this.state, autoOuterGoal: x}); }
    setAutoInnerGoal = (x: number) => { this.handleChange({...this.state, autoInnerGoal: x}); }
    setAutoBallsMissed = (x: number) => { this.handleChange({...this.state, autoBallsMissed: x}); }

    setTeleopBallsIn = (x: number) => { this.handleChange({...this.state, teleopBallsIn: x}); }
    setTeleopLowGoal = (x: number) => { this.handleChange({...this.state, teleopLowGoal: x}); }
    setTeleopOuterGoal = (x: number) => { this.handleChange({...this.state, teleopOuterGoal: x}); }
    setTeleopInnerGoal = (x: number) => { this.handleChange({...this.state, teleopInnerGoal: x}); }
    setTeleopBallsMissed = (x: number) => { this.handleChange({...this.state, teleopBallsMissed: x}); }

    setClimb = (x: ClimbResult) => { this.handleChange({...this.state, climb: x}); }

    render() {
        return (
            <div className="scoresheet ">
                <h6>Auto</h6>
                <Counter fieldName="Balls In" onChange={this.setAutoBallsIn} startingAmount={this.state.autoBallsIn} minimum={0} />
                <Counter fieldName="Inner Goal" onChange={this.setAutoInnerGoal} startingAmount={this.state.autoInnerGoal} minimum={0} />
                <Counter fieldName="Outer Goal" onChange={this.setAutoOuterGoal} startingAmount={this.state.autoOuterGoal} minimum={0} />
                <Counter fieldName="Lower Goal" onChange={this.setAutoLowGoal} startingAmount={this.state.autoLowGoal} minimum={0} />
                <Counter fieldName="Balls Missed" onChange={this.setAutoBallsMissed} startingAmount={this.state.autoBallsMissed} minimum={0} />
                <hr/>
                <h6>Teleop</h6>
                <Counter fieldName="Balls In" onChange={this.setTeleopBallsIn} startingAmount={this.state.teleopBallsIn} minimum={0} />
                <Counter fieldName="Inner Goal" onChange={this.setTeleopInnerGoal} startingAmount={this.state.teleopInnerGoal} minimum={0} />
                <Counter fieldName="Outer Goal" onChange={this.setTeleopOuterGoal} startingAmount={this.state.teleopOuterGoal} minimum={0} />
                <Counter fieldName="Lower Goal" onChange={this.setTeleopLowGoal} startingAmount={this.state.teleopLowGoal} minimum={0} />
                <Counter fieldName="Balls Missed" onChange={this.setTeleopBallsMissed} startingAmount={this.state.teleopBallsMissed} minimum={0} />
                <hr/>
                <h6>Endgame</h6>
                <div className="climbtoolbar">
                    <ButtonToolbar>
                        <ToggleButtonGroup name="endgame" onChange={this.setClimb} defaultValue={this.state.climb}>
                            <ToggleButton variant="outline-dark" value={ClimbResult.None}>None</ToggleButton>
                            <ToggleButton variant="outline-danger" value={ClimbResult.Park}>Parked</ToggleButton>
                            <ToggleButton variant="outline-primary" value={ClimbResult.ClimbMissed}>Missed</ToggleButton>
                            <ToggleButton variant="outline-success" value={ClimbResult.ClimbSuccess}>Climbed</ToggleButton>
                        </ToggleButtonGroup>
                    </ButtonToolbar>
                </div>
            </div>
        )
    }
}

function defaultState(): State {
    return {
        autoBallsIn: 3,
        autoLowGoal: 0,
        autoOuterGoal: 0,
        autoInnerGoal: 0,
        autoBallsMissed: 0,
        teleopBallsIn: 0,
        teleopLowGoal: 0,
        teleopOuterGoal: 0,
        teleopInnerGoal: 0,
        teleopBallsMissed: 0,
        climb: ClimbResult.None
    }
}