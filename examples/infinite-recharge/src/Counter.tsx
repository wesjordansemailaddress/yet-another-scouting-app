import React = require("react");
import "../style/Counter.sass";
import { Log } from "./util";

import Button from "react-bootstrap/Button";
import Badge from "react-bootstrap/Badge";

const TAG = "src/Counter.tsx"

export interface Props {
    fieldName: String;
    onChange: (newValue: number) => void;
    startingAmount?: number;
    minimum?: number;
    maximum?: number;
}

export interface State {
    amount: number;
}

export class Counter extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { amount: props.startingAmount || 0 };
    }

    updateAmount = (currentAmount: number) => {
        if (currentAmount > this.props.maximum ?? 99999) {
            Log.w(TAG, `${this.props.fieldName} reached maximum value of ${this.props.maximum}`)
            return;
        }

        if (currentAmount < this.props.minimum ?? -99999) {
            Log.w(TAG, `${this.props.fieldName} reached minimum value of ${this.props.minimum}`)
            return;
        }

        this.setState({ amount: currentAmount });
        this.props.onChange(currentAmount);
    }

    onPlus = () => this.updateAmount(this.state.amount + 1);
    onMinus = () => this.updateAmount(this.state.amount - 1);

    render() {
        return (
            <div className="counter">
                <Button variant="danger" onClick={this.onMinus}>-</Button>
                <div className="fieldName">
                    <span>{ this.props.fieldName }: </span>
                    <Badge variant="secondary">{ this.state.amount }</Badge>
                </div>
                <Button variant="primary" onClick={this.onPlus}>+</Button>
            </div>
        )
    }
}