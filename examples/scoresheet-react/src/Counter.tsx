import React = require("react");
import "../style/Counter.sass";
import { Log } from "./util";

const TAG = "src/Counter.tsx"

export interface Props {
    fieldName: String;
    onChange: (newValue: number) => void;
    startingAmount?: number;
    minimum?: number;
    maximum?: number;
}

export interface State {
    amount: number;
}

export class Counter extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { amount: props.startingAmount || 0 };
    }

    updateAmount = (currentAmount: number) => {
        if (currentAmount > this.props.maximum ?? 99999) {
            Log.w(TAG, `${this.props.fieldName} reached maximum value of ${this.props.maximum}`)
            return;
        }

        if (currentAmount < this.props.minimum ?? -99999) {
            Log.w(TAG, `${this.props.fieldName} reached minimum value of ${this.props.minimum}`)
            return;
        }

        this.setState({ amount: currentAmount });
        this.props.onChange(currentAmount);
    }

    onPlus = () => this.updateAmount(this.state.amount + 1);
    onMinus = () => this.updateAmount(this.state.amount - 1);

    render() {
        return (
            <div className="counter">
                <button onClick={this.onMinus}>-</button>
                <div className="fieldName">
                    <span>{ this.props.fieldName }: </span>
                    <b>{ this.state.amount }</b>
                </div>
                <button onClick={this.onPlus}>+</button>
            </div>
        )
    }
}