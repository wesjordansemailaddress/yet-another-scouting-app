import React = require("react");
import { Counter } from "./Counter";
import { Log } from "./util";

const TAG = "src/Scoresheet.tsx"

export interface Props {
    getState?: () => State | null,
    onChange: (newState: State) => void
}

export interface State {
    hatches: number,
    cargo: number,
    startLevel: number,
    endLevel: number
}

export class Scoresheet extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        if (props.getState) {
            const importedState = props.getState();
            if (importedState) {
                this.state = importedState
                return
            }
        }

        this.state = defaultState()
    }

    handleChange(newState: State) {
        Log.i(TAG, newState)
        this.props.onChange(newState)
        this.setState(newState)
    }

    setHatches = (newValue: number) => {
        this.handleChange({ ...this.state, hatches: newValue })
    }

    setCargo = (newValue: number) => {
        this.handleChange({ ...this.state, cargo: newValue })
    }

    setStartLevel = (newValue: number) => {
        this.handleChange({ ...this.state, startLevel: newValue })
    }

    setEndLevel = (newValue: number) => {
        this.handleChange({ ...this.state, endLevel: newValue })
    }

    render() {
        return (
            <div className="scoresheet">
                <Counter fieldName="Hatches Placed" onChange={this.setHatches} startingAmount={this.state.hatches} minimum={0} />
                <Counter fieldName="Cargo Placed" onChange={this.setCargo} startingAmount={this.state.cargo} minimum={0}/>
                <Counter fieldName="Starting HAB Level" onChange={this.setStartLevel} startingAmount={this.state.startLevel} minimum={0} maximum={2}/>
                <Counter fieldName="Ending HAB level" onChange={this.setEndLevel} startingAmount={this.state.startLevel} minimum={0} maximum={3} />
            </div>
        )
    }
}

function defaultState(): State {
    return {
        hatches: 0,
        cargo: 0,
        startLevel: 0,
        endLevel: 0
    }
}