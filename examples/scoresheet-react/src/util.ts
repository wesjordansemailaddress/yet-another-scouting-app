export enum LogPriority {
    WTF = "WTF", ERROR = "E", WARNING = "W", INFO = "I", DEBUG = "D", TRACE = "T"
}

export const Log = {
    log(priority:LogPriority, tag:string, stuff: any) {
        const text = `[${tag}] ${priority} | ${JSON.stringify(stuff)}`;
        if (priority == LogPriority.ERROR) {
            console.error(text, stuff);
        } else if (priority == LogPriority.WARNING) {
            console.warn(text, stuff);
        } else if (priority == LogPriority.INFO) {
            console.info(text, stuff);
        } else if (priority == LogPriority.DEBUG) {
            console.debug(text, stuff);
        } else if (priority == LogPriority.TRACE) {
            console.trace(text, stuff);
        } else {
            console.log(text, stuff)
        }
    },

    wtf(tag:string, stuff: any) {
        this.log(LogPriority.WTF, tag, stuff);
    },

    e(tag:string, stuff: any) {
        this.log(LogPriority.ERROR, tag, stuff);
    },

    w(tag:string, stuff: any) {
        this.log(LogPriority.WARNING, tag, stuff);
    },

    i(tag:string, stuff: any) {
        this.log(LogPriority.INFO, tag, stuff);
    },

    d(tag:string, stuff: any) {
        this.log(LogPriority.DEBUG, tag, stuff);
    },

    t(tag:string, stuff: any) {
        this.log(LogPriority.TRACE, tag, stuff);
    },
}