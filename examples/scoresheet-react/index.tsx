import React from 'react'
import ReactDOM from 'react-dom'

import { Scoresheet, State } from './src/Scoresheet'
import { Log } from './src/util';

const TAG = "index.tsx"

// These definitions are pulled in from WebView and call Java code.
// Don't move this, or else Typescipt/Parcel will try and mangle them.
declare var YASA: {
  loadData(): string,
  saveData(data: string): void,
  apiVersion: string
}

function save(data:State) {
  Log.i(TAG, "Saving data from YASA");
  Log.i(TAG, data);

  const data_str = JSON.stringify(data);
  //YASA.saveData(data_str);
}

function load(): State | null {
  const data_str = null; //YASA.loadData();
  Log.i(TAG, "Pulled data from YASA");
  Log.i(TAG, data_str);

  if (data_str) {
    const data: State = JSON.parse(data_str);
    return data;
  } else  {
    return null;
  }
}

ReactDOM.render(
  <Scoresheet onChange={save} getState={load}/>,
  document.getElementById('root'),
)