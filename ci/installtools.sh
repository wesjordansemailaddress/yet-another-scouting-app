#!/usr/bin/env bash

set -eux

if [[ ! -d $ANDROID_HOME ]]; then
  apt-get --quiet update --yes
  apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1
  wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
  unzip -d $ANDROID_HOME android-sdk.zip
  echo y | $ANDROID_HOME/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
  echo y | $ANDROID_HOME/tools/bin/sdkmanager "platform-tools" >/dev/null
  echo y | $ANDROID_HOME/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
fi

yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses